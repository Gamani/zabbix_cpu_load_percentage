#!/bin/bash
cn=$1
echo q | htop -C | aha --line-fix | html2text -width 999 |
grep -v "F1Help\|xml version=" > /tmp/file.txt
cat /tmp/file.txt | grep %] | sed s/[^0-9[:space:].]//g | awk {'print $2'} > /tmp/core1.log
cat /tmp/file.txt | grep %] | sed s/[^0-9[:space:].]//g | awk {'print $4'} > /tmp/core2.log
# cat /tmp/core1.log /tmp/core2.log
if [ $1 -eq 1 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n1
fi
if [ $1 -eq 2 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n2 | tail -1
fi
if [ $1 -eq 3 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n3 | tail -1
fi
if [ $1 -eq 4 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n4 | tail -1
fi
if [ $1 -eq 5 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n5 | tail -1
fi
if [ $1 -eq 6 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n6 | tail -1
fi
if [ $1 -eq 7 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n7 | tail -1
fi
if [ $1 -eq 8 ]; then
    cat /tmp/core1.log /tmp/core2.log | head -n8 | tail -1
fi
